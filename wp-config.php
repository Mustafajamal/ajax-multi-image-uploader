<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ajax_multifile' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'HD^,LlTM&!]kIE$bufd_=)^-JdVG *TvRDm-+>YP5|E^$a/^EV!|NxiFB%h3R^0-' );
define( 'SECURE_AUTH_KEY',  'PNCJyBg<v..5LY}h9oxm$spI..uO0Y9a5gp|q6Xh:?/{c{Oa.ua+#Ot|i-.jJyWW' );
define( 'LOGGED_IN_KEY',    'yIU.%j.5/:!~q<u3d!%FRXRi(.JGZxhZ~6vAT0Q;|ds>m$uzoz:f(w.-44!;O0R[' );
define( 'NONCE_KEY',        ',v51^pSd_GMPT/MQ[^I|67q&|[&$$lzi`o|jmc`+]8.*DLLv3CU{1F3)cCabk8%<' );
define( 'AUTH_SALT',        '8jcxKJ7!fteqiKNSF,!Q%R(n3tgp.QPz,77,jfV }8]AS$Q,dfdz~N1,iWvmq5>S' );
define( 'SECURE_AUTH_SALT', 'Qir<Hh2^vB@;ZgEA(fwjGB@Y;1%HT0-+%a^wU@*1XmI>qZ/:9P#rQvkLtiC.^GTr' );
define( 'LOGGED_IN_SALT',   '^%e-&Z,gK|+T n`I^hv+?NG0a2*xa{>K.98&:SdZY]78tOd+o}AeU+XKJsEKJI)}' );
define( 'NONCE_SALT',       '!psL!R;}Pg|PbZ&R0LI:aBX)T$N@4iv{FlZ4-C6)u^~(U(YH}Aiq~sTnf[28t>@ ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
